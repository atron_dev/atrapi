from flask import request
from flask_restful import Resource
from Model import db, Witelocc, WiteloccSchema


witelocc_schema = WiteloccSchema(only=('lessthan50','from50to70','morethan70','total'),many=True)

class WiteloccResource(Resource):
    def get(self):
        ridar_counter = Witelocc.query.filter_by(witel='ridar').all()
        ridar_counter = witelocc_schema.dump(ridar_counter).data
        ridar_result = {'witel':'RIDAR','contents': ridar_counter}

        medan_counter = Witelocc.query.filter_by(witel='medan').all()
        medan_counter = witelocc_schema.dump(medan_counter).data
        medan_result = {'witel':'MEDAN','contents': medan_counter}

        sumsel_counter = Witelocc.query.filter_by(witel='sumsel').all()
        sumsel_counter = witelocc_schema.dump(sumsel_counter).data
        sumsel_result = {'witel':'SUMSEL','contents': sumsel_counter}
        # print(occupancy)
        return {"data":[ridar_result,medan_result,sumsel_result]}, 200
