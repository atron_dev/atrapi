from flask import request
from flask_restful import Resource, reqparse
from Model import db, Bandwidth, BandwidthSchema

bandwidth_schema = BandwidthSchema(only=('site_id', 'site_name','witel','bw_tps','agent_host'),many=True)
bw_schema = BandwidthSchema()
#
# parser = reqparse.RequestParser()
# parser.add_argument('status',type=str)
# parser.add_argument('witel',type=str)
# parser.add_argument('current_occ',type=int)

class BandwidthResource(Resource):
    def get(self):
        # args = parser.parse_args()
        # args_status = args['status']
        # args_witel = args['witel']  # Witel specific
        # args_current_occ = args['current_occ'] # Boolean

        bw_data = Bandwidth.query.order_by(Bandwidth.site_id.desc()).all()
        bw_data = bandwidth_schema.dump(bw_data).data
        return {"data":bw_data}, 200

    def post(self):
        json_data = request.get_json(force=True)
        # print(json_data['agent_host'])
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = bw_schema.load(json_data)
        # print(data['agent_host'])
        # print(errors)

        if errors:
            return errors, 422
        check_siteid = Bandwidth.query.filter_by(site_id=data['site_id']).first()
        check_agenthost = Bandwidth.query.filter_by(agent_host=data['agent_host']).first()
        if check_siteid:
            return {'message': 'Site ID already exists'}, 400
        if check_agenthost:
            return {'message': 'Agent Host already exists'}, 400

        bandwidth = Bandwidth(
            site_id = json_data['site_id'],
            site_name = json_data['site_name'],
            witel = json_data['witel'],
            agent_host = json_data['agent_host'],
            bw_tps = json_data['bw_tps']
            )

        # print(bandwidth)
        db.session.add(bandwidth)
        db.session.commit()

        result = bw_schema.dump(bandwidth).data

        return { "status": 'success', 'data': result }, 201

    def put(self):
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = bw_schema.load(json_data)
        # print(data)
        if errors:
            return errors, 422
        bandwidth = Bandwidth.query.filter_by(site_id=data['site_id']).first()
        if not bandwidth:
            return {'message': 'Site ID does not exist'}, 400
        bandwidth.bw_tps = data['bw_tps']
        db.session.commit()

        result = bw_schema.dump(bandwidth).data
        return { "status": 'success', 'data': result }, 200

    def delete(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = bw_schema.load(json_data)
        if errors:
            return errors, 422
        bandwidth = Bandwidth.query.filter_by(site_id=data['site_id']).delete()
        db.session.commit()

        result = bw_schema.dump(bandwidth).data

        return { "status": 'success', 'data': result}, 200
