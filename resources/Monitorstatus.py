from flask import request
from flask_restful import Resource
from Model import db, Monitorstatus, MonitorstatusSchema

monitorstatus_schema = MonitorstatusSchema(only=('up', 'down'),many=True)


class MonitorstatusResource(Resource):
    def get(self):
        mon_stat = Monitorstatus.query.all()
        mon_stat = monitorstatus_schema.dump(mon_stat).data

        return {"data": mon_stat}, 200
