from flask import request
from flask_restful import Resource, reqparse
from Model import db, Topridar, TopridarSchema, Topmedan, TopmedanSchema, Topsumsel, TopsumselSchema

topridar_schema = TopridarSchema(only=('site_id', 'site_name','port_uplink','status','bandwidth','current_occ','daily_occ','weekly_occ','monthly_occ','yearly_occ'),many=True)
topmedan_schema = TopmedanSchema(only=('site_id', 'site_name','port_uplink','status','bandwidth','current_occ','daily_occ','weekly_occ','monthly_occ','yearly_occ'),many=True)
topsumsel_schema = TopsumselSchema(only=('site_id', 'site_name','port_uplink','status','bandwidth','current_occ','daily_occ','weekly_occ','monthly_occ','yearly_occ'),many=True)

parser = reqparse.RequestParser()
parser.add_argument('status',type=str)
parser.add_argument('witel',type=str)
parser.add_argument('current_occ',type=int)

class SummaryoccResource(Resource):
    def get(self):
        args = parser.parse_args()
        args_status = args['status']
        args_witel = args['witel']  # Witel specific
        args_current_occ = args['current_occ'] # Boolean

        if args_status == 'up':
            if args_witel == 'ridar':
                if args_current_occ == 1:
                    ridar_occ = Topridar.query.filter(Topridar.current_occ<50).all()
                elif args_current_occ == 2:
                    ridar_occ = Topridar.query.filter((Topridar.current_occ>=50)&(Topridar.current_occ<=70)).all()
                elif args_current_occ == 3:
                    ridar_occ = Topridar.query.filter(Topridar.current_occ>70).all()
                else:
                    return {"error":"group not found"}, 404
                ridar_occ = topridar_schema.dump(ridar_occ).data
                ridar_result = {'witel':'RIDAR','tsel_region':'SUMBAGTENG','contents': ridar_occ}
                return {"data":[ridar_result]}, 200

            elif args_witel == 'medan':
                if args_current_occ == 1:
                    medan_occ = Topmedan.query.filter(Topmedan.current_occ<50).all()
                elif args_current_occ == 2:
                    medan_occ = Topmedan.query.filter((Topmedan.current_occ>=50)&(Topmedan.current_occ<=70)).all()
                elif args_current_occ == 3:
                    medan_occ = Topmedan.query.filter(Topmedan.current_occ>70).all()
                else:
                    return {"error":"group not found"}, 404
                medan_occ = topmedan_schema.dump(medan_occ).data
                medan_result = {'witel':'MEDAN','tsel_region':'SUMBAGUT','contents': medan_occ}
                return {"data":[medan_result]}, 200

            elif args_witel == 'sumsel':
                if args_current_occ == 1:
                    sumsel_occ = Topsumsel.query.filter(Topsumsel.current_occ<50).all()
                elif args_current_occ == 2:
                    sumsel_occ = Topsumsel.query.filter((Topsumsel.current_occ>=50)&(Topsumsel.current_occ<=70)).all()
                elif args_current_occ == 3:
                    sumsel_occ = Topsumsel.query.filter(Topsumsel.current_occ>70).all()
                else:
                    return {"error":"group not found"}, 404
                sumsel_occ = topsumsel_schema.dump(sumsel_occ).data
                sumsel_result = {'witel':'SUMSEL','tsel_region':'SUMBAGSEL','contents': sumsel_occ}
                return {"data":[sumsel_result]}, 200

            else:
                return {"error":"witel not found"}, 404
        else:
            ridar_occ = Topridar.query.order_by(Topridar.current_occ.desc()).all()
            ridar_occ = topridar_schema.dump(ridar_occ).data
            ridar_result = {'witel':'RIDAR','tsel_region':'SUMBAGTENG','contents': ridar_occ}

            medan_occ = Topmedan.query.order_by(Topmedan.current_occ.desc()).all()
            medan_occ = topmedan_schema.dump(medan_occ).data
            medan_result = {'witel':'MEDAN','tsel_region':'SUMBAGUT','contents': medan_occ}

            sumsel_occ = Topsumsel.query.order_by(Topsumsel.current_occ.desc()).all()
            sumsel_occ = topsumsel_schema.dump(sumsel_occ).data
            sumsel_result = {'witel':'SUMSEL','tsel_region':'SUMBAGSEL','contents': sumsel_occ}
            # print(occupancy)
            return {"data":[ridar_result,medan_result,sumsel_result]}, 200
