from flask import Blueprint
from flask_restful import Api
from resources.Summaryocc import SummaryoccResource
from resources.Witelocc import WiteloccResource
from resources.Topocc import TopoccResource
from resources.Nodestatus import NodestatusResource
from resources.Bandwidth import BandwidthResource
api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Routes

api.add_resource(SummaryoccResource, '/summaryocc')
api.add_resource(WiteloccResource, '/witelocc')
api.add_resource(TopoccResource, '/topocc')
api.add_resource(NodestatusResource, '/nodestatus')
api.add_resource(BandwidthResource, '/bwconfig')
